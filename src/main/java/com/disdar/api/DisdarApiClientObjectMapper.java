package com.disdar.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;


public class DisdarApiClientObjectMapper {

    private DisdarApiClientObjectMapper() {
    }

    public static ObjectMapper newObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        return configure(mapper);
    }

    private static ObjectMapper configure(ObjectMapper mapper) {

        // configure Jackson to serialize Date in ISO8601 format
        mapper.setDateFormat(new ISO8601DateFormat());

        // Add JSON serializers and deserializers for the Java 8 time API
        mapper.registerModule(new JavaTimeModule());

        // In order to make the API client stable towards additional label types
        // that might be added in future versions of the API we configure Jackson
        // to represent unknown enum values as null (instead of failing with an exception
        // as is the default setting)
        mapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);

        return mapper;
    }
}

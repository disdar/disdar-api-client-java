package com.disdar.api.model;

import com.disdar.api.model.label.Label;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Extraction {

    private String value;
    private int page;
    private List<Label> labels;

    @JsonCreator
    public Extraction(@JsonProperty("value") String value,
                      @JsonProperty("page") int page,
                      @JsonProperty("labels") List<Label> labels) {
        this.value = value;
        this.page = page;
        this.labels = labels;
    }

    public String getValue() {
        return value;
    }

    public int getPage() {
        return page;
    }

    public List<Label> getLabels() {
        return labels;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return value;
    }
}

package com.disdar.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.net.URL;
import java.util.List;

/**
 * A class storing the results of the extraction process.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExtractionResult {

    private String id;
    private double confidence;
    private List<Extraction> extractions;
    private List<URL> rasterizedPages;

    @JsonCreator
    public ExtractionResult(@JsonProperty("id") String id,
                            @JsonProperty("confidence") double confidence,
                            @JsonProperty("extractions") List<Extraction> extractions,
                            @JsonProperty("rasterizedPages") List<URL> rasterizedPages) {
        this.id = id;
        this.confidence = confidence;
        this.extractions = extractions;
        this.rasterizedPages = rasterizedPages;
    }

    public String getId() {
        return id;
    }

    public double getConfidence() {
        return confidence;
    }

    public List<Extraction> getExtractions() {
        return extractions;
    }

    public List<URL> getRasterizedPages() {
        return rasterizedPages;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return String.format("ExtractionResult: %s", id);
    }
}

package com.disdar.api.model.label;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LabelString extends Label {

    private final String value;

    @JsonCreator
    public LabelString(@JsonProperty("value") String value, @JsonProperty("type") LabelType type, @JsonProperty("confidence") double confidence) {
        super(type, confidence);
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

package com.disdar.api.model.label;

public enum LabelType {

    // Note: the Java type we use to hold the value of a  LabelType
    // is defined using Jackson annotations in the Label base class

    AMOUNT,
    IBAN,
    BIC,
    INVOICEDATE,
    INVOICENUMBER,
    CREDITORNAME,
    TAXRATE,
    NETAMOUNT,
    LOGO,
    TAXID,
    VATID
}
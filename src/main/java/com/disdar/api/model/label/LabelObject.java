package com.disdar.api.model.label;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Label that is used to represent labels that are unknown to the current version of the API client,
 * i.e. all labels that do not appear in the LabelType enum.
 *
 * The reason why this class exists is to make the API client stable to future changes/additions in
 * the labels the DISDAR API extracts.
 *
 * Note that this label's 'type' property is always null (because for unknown labels the client does
 * not have exact information how the label is suposed to be represented).
 */
public class LabelObject extends Label {

    private final Object value;

    @JsonCreator
    public LabelObject(@JsonProperty("value") Object value, @JsonProperty("type") LabelType type, @JsonProperty("confidence") double confidence) {
        super(type, confidence);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }
}
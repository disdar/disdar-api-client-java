package com.disdar.api.model.label;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static com.fasterxml.jackson.annotation.JsonSubTypes.Type;


/**
 * Make Jackson instantiate a subclass of Label depending on the value of the 'type' property.
 *
 * In case Jackson encounters an unknown value for 'type' we configure it to map all those case
 * to an instance of LabelObject (which in turn represents the value as a Java Object). We do this
 * in order to make the API client resilient to future changes in the label types.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", visible = true, defaultImpl = LabelObject.class)
@JsonSubTypes({
        @Type(value = LabelString.class, name = "IBAN"),
        @Type(value = LabelString.class, name = "BIC"),
        @Type(value = LabelString.class, name = "INVOICENUMBER"),
        @Type(value = LabelLong.class, name = "AMOUNT"),
        @Type(value = LabelLocalDate.class, name = "INVOICEDATE"),
        @Type(value = LabelString.class, name = "CREDITORNAME"),
        @Type(value = LabelLong.class, name = "NETAMOUNT"),
        @Type(value = LabelBigDecimal.class, name = "TAXRATE"),
        @Type(value = LabelString.class, name = "LOGO"),
        @Type(value = LabelString.class, name = "TAXID"),
        @Type(value = LabelString.class, name = "VATID")
})
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Label implements Comparable<Label> {


    private LabelType type;
    private double confidence;

    @JsonCreator
    public Label(@JsonProperty("type") LabelType type,
                 @JsonProperty("confidence") double confidence) {
        this.type = type;
        this.confidence = confidence;
    }

    public LabelType getType() {
        return type;
    }

    public double getConfidence() {
        return confidence;
    }

    @Override
    public int compareTo(Label o) {
        return type.compareTo(o.getType());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Label label = (Label) o;
        return java.util.Objects.equals(type, label.type);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return type + " - " + confidence;
    }
}
package com.disdar.api.model.label;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class LabelLong extends Label {

    private final long value;

    @JsonCreator
    public LabelLong(@JsonProperty("value") long value, @JsonProperty("type") LabelType type, @JsonProperty("confidence") double confidence) {
        super(type, confidence);
        this.value = value;
    }

    public long getValue() {
        return value;
    }
}

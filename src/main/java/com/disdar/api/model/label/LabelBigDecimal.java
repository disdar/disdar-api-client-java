package com.disdar.api.model.label;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;


public class LabelBigDecimal extends Label {

    private final BigDecimal value;

    @JsonCreator
    public LabelBigDecimal(@JsonProperty("value") BigDecimal value, @JsonProperty("type") LabelType type, @JsonProperty("confidence") double confidence) {
        super(type, confidence);
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }
}

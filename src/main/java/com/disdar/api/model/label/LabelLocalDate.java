package com.disdar.api.model.label;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class LabelLocalDate extends Label {

    private final LocalDate value;

    @JsonCreator
    public LabelLocalDate(@JsonProperty("value") LocalDate value, @JsonProperty("type") LabelType type, @JsonProperty("confidence") double confidence) {
        super(type, confidence);
        this.value = value;
    }

    public LocalDate getValue() {
        return value;
    }
}

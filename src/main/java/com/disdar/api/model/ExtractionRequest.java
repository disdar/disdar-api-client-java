package com.disdar.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.net.URL;

/**
 * A class encapsulating all incoming arguments needed for the extraction process.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExtractionRequest {

    private URL fileURL;
    private boolean rasterizedPagesFlag;

    public ExtractionRequest(URL fileURL, boolean rasterizedPagesFlag) {
        this.fileURL = fileURL;
        this.rasterizedPagesFlag = rasterizedPagesFlag;
    }

    public URL getFileURL() {
        return fileURL;
    }

    public boolean returnsRasterizedPages() {
        return rasterizedPagesFlag;
    }

    public void setFileURL(URL fileURL) {
        this.fileURL = fileURL;
    }

    public void setRasterizedPagesFlag(boolean rasterizedPagesFlag) {
        this.rasterizedPagesFlag = rasterizedPagesFlag;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}

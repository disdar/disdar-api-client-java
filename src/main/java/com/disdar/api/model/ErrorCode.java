package com.disdar.api.model;

/**
 * An enumeration class for error codes.
 */
public enum ErrorCode {

    // errors that are caused by the client providing "wrong" data
    BAD_REQUEST,
    FILE_NOT_ACCESSIBLE,
    UNSUPPORTED_MEDIA_TYPE,
    FILE_SIZE_LIMIT,
    PAGE_COUNT_LIMIT,
    FORBIDDEN,
    UNPROCESSABLE_ENTITY,
    TOO_MANY_REQUESTS,

    INVALID_CREDENTIALS,

    // error caused by a bug in the extraction pipeline
    INTERNAL_ERROR
}
/*
 * Copyright (c) 2016, DISDAR GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   - Neither the name of the DISDAR GmbH nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DISDAR GmbH BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.disdar.api.client;

import com.disdar.api.DisdarApiClientObjectMapper;
import com.disdar.api.model.ErrorCode;
import com.disdar.api.model.ErrorResult;
import com.disdar.api.model.ExtractionRequest;
import com.disdar.api.model.ExtractionResult;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * A class for performing extraction requests to the DISDAR API.
 */
public class DisdarApiClient {

    private static final String AUTHENTICATION_HEADER = "x-api-key";
    private static final String BASE_URL = "https://api.disdar.com/v2/";

    private static final ObjectMapper objectMapper = DisdarApiClientObjectMapper.newObjectMapper();

    private final Client client;
    private final String apiKey;

    /**
     * Create a client capable of sending extraction requests to the DISDAR API.
     *
     * @param apiKey      Your API key
     */
    public DisdarApiClient(String apiKey) {
        this.client = createClient();
        this.apiKey = apiKey;
    }

    /**
     * Send an extraction request to the DISDAR API.
     *
     * @param extractionRequest The extraction request
     * @throws DisdarServiceException if the extraction service returns a non 200 response
     */
    public ExtractionResult extract(ExtractionRequest extractionRequest) throws IOException {

        String target = BASE_URL + "extractions";
        if(extractionRequest.returnsRasterizedPages()) target += "?rasterizedPages=true";

        Invocation.Builder invocationBuilder = client.target(target).request(MediaType.APPLICATION_JSON);
        invocationBuilder.header(AUTHENTICATION_HEADER, apiKey);

        Response response = invocationBuilder.post(Entity.json("{\"fileURL\":\"" + extractionRequest.getFileURL() + "\"}"));

        // By definition the DISDAR API provides two different return value types:
        //
        // 1) successful response
        if (response.getStatus() == 200) {
            String res = response.readEntity(String.class);
            return objectMapper.readValue(res, ExtractionResult.class);
        }
        // 2) Invalid credentials
        //
        // This case is a slightly different than all other error cases because the
        // DISDAR API does *not* return an ErrorResult JSON body for this specific case
        else if (response.getStatus() == 403) {
            throw new DisdarServiceException(ErrorCode.INVALID_CREDENTIALS, "The credentials you provided were invalid or missing");
        }
        // 3) error response, map to an DisdarServiceException
        else {
            ErrorResult result = response.readEntity(ErrorResult.class);
            throw new DisdarServiceException(result.getErrorCode(), result.getErrorMessage());
        }
    }


    /**
     * Create a pre-configured client capable of handling secure requests to the DISDAR API.
     *
     * @return A Jersey client configured to trust the DISDAR SSL certificate
     */
    private static Client createClient() {

        try {
            KeyStore trustStore = KeyStore.getInstance("JKS");
            trustStore.load(DisdarApiClient.class.getResourceAsStream("/truststore.jks"), "disdar".toCharArray());
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("SunX509");
            trustManagerFactory.init(trustStore);

            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());

            return ClientBuilder.newBuilder()
                    .sslContext(sslContext)
                    .build();
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException | CertificateException e) {
            throw new DisdarApiClientException("Unable to build SSL context for the Jersey client.", e);
        }
    }
}
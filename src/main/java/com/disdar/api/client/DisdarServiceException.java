package com.disdar.api.client;

import com.disdar.api.model.ErrorCode;


/**
 * An exception class that represents all error cases that may happen if
 * an extraction task is being processed by the DISDAR API.
 */
public class DisdarServiceException extends RuntimeException {

    private ErrorCode errorCode;
    private String message;

    public DisdarServiceException(ErrorCode errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.message = errorMessage;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
/*
 * Copyright (c) 2014, DISDAR GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   - Neither the name of the DISDAR GmbH nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DISDAR GmbH BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.disdar.api.client;

import com.disdar.api.model.ErrorCode;
import com.disdar.api.model.ExtractionRequest;
import com.disdar.api.model.ExtractionResult;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests for the DISDAR client.
 */
public class DisdarClientTest {


    static final DisdarApiClient clientWithInvalidCredentials = new DisdarApiClient("invalidapikey");
    static final DisdarApiClient client = new DisdarApiClient("nHWQsgiuNT30dhAJjUg9824cCpCdZyHyAUQWwtPc");

    @Test
    public void testSSLConfiguration() throws IOException {

        /*
         * Test if the configured Jersey client is able to send secure requests to the DISDAR API. As we
         * are not sending any credentials we expect to get a 403 response. This indicates that the SSL
         * connection works because HTTP works on top of SSL.
         */

        ExtractionRequest extractionRequest = new ExtractionRequest(new URL("https://www.disdar.com/doesnotexist.pdf"), false);

        try {
            clientWithInvalidCredentials.extract(extractionRequest);
        } catch (DisdarServiceException e) {
            assertEquals(e.getErrorCode(), ErrorCode.INVALID_CREDENTIALS);
        }
    }

    @Test
    public void testValidRequestWorks() throws IOException {
        ExtractionRequest request = new ExtractionRequest(new URL("https://s3-eu-west-1.amazonaws.com/extraction-service-test/0.png"), true);
        ExtractionResult result = client.extract(request);
        assertNotNull(result);
        assertTrue(result.getConfidence() > 0);
        assertTrue(result.getExtractions().size() > 0);
    }
}

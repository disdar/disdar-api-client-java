package com.disdar.api.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.junit.Assert;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Mix-in providing test patterns commonly used in serialization/deserialization test.
 */
public interface SerializationTest {

    /**
     * Encapsulates the common pattern when testing serialization:
     * 1. Serialize object to String using Jackson
     * 2. Deserialize from String generated in step 1
     * 3. Compare equality of original and deserialized object
     */
    default <T> T testSerializeThenDeserialize(ObjectMapper objectMapper, Object object, Class<T> valueType) throws IOException {


        final String serializedString = objectMapper.writeValueAsString(object);
        System.out.println(serializedString);
        Assert.assertNotNull(serializedString);
        final T deserializedObject = objectMapper.readValue(serializedString, valueType);
        Assert.assertNotNull(deserializedObject);

        // TODO: this is a reasonable test but would require us to
        // implement equals on all involved objects. Need to decide
        // how/if we want to do that
        //Assert.assertEquals(object, deserializedObject);
        return deserializedObject;
    }


    default String fixture(String filename) {
        return fixture(filename, Charsets.UTF_8);
    }

    default String fixture(String filename, Charset charset) {
        try {
            return Resources.toString(Resources.getResource(filename), charset).trim();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

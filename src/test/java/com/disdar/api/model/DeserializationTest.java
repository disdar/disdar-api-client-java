/*
 * Copyright (c) 2014, DISDAR GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   - Neither the name of the DISDAR GmbH nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DISDAR GmbH BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.disdar.api.model;

import com.disdar.api.DisdarApiClientObjectMapper;
import com.disdar.api.model.label.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;


public class DeserializationTest implements SerializationTest {


    private static final ObjectMapper om = DisdarApiClientObjectMapper.newObjectMapper();

    @Test
    public void testDeserialization() throws IOException {

        testLabelDeserialization("LabelIBAN.json", LabelString.class, LabelType.IBAN);
        testLabelDeserialization("LabelBIC.json", LabelString.class, LabelType.BIC);
        testLabelDeserialization("LabelInvoiceNumber.json", LabelString.class, LabelType.INVOICENUMBER);
        testLabelDeserialization("LabelLogo.json", LabelString.class, LabelType.LOGO);
        testLabelDeserialization("LabelCreditorName.json", LabelString.class, LabelType.CREDITORNAME);

        testLabelDeserialization("LabelTaxId.json", LabelString.class, LabelType.TAXID);
        testLabelDeserialization("LabelVatId.json", LabelString.class, LabelType.VATID);

        testLabelDeserialization("LabelAmount-1.json", LabelLong.class, LabelType.AMOUNT);
        testLabelDeserialization("LabelAmount-2.json", LabelLong.class, LabelType.AMOUNT);
        testLabelDeserialization("LabelAmount-3.json", LabelLong.class, LabelType.AMOUNT);

        testLabelDeserialization("LabelNetAmount-1.json", LabelLong.class, LabelType.NETAMOUNT);
        testLabelDeserialization("LabelNetAmount-2.json", LabelLong.class, LabelType.NETAMOUNT);
        testLabelDeserialization("LabelNetAmount-3.json", LabelLong.class, LabelType.NETAMOUNT);

        testLabelDeserialization("LabelTaxRate.json", LabelBigDecimal.class, LabelType.TAXRATE);

        testLabelDeserialization("LabelInvoiceDate-1.json", LabelLocalDate.class, LabelType.INVOICEDATE);
        testLabelDeserialization("LabelInvoiceDate-2.json", LabelLocalDate.class, LabelType.INVOICEDATE);

        testLabelDeserialization("LabelUnknown-1.json", LabelObject.class, null);

        // All "unsupported" labels, i.e. labels that are added to the API after this
        // version of the client has been released are expected to map to a generic
        // Label object with its 'type' property set to null (there's no way for the
        // client to make safe assumptions about the correct Java type for an unknown label)
        testLabelDeserialization("LabelUnknown-1.json", LabelObject.class, null);
        testLabelDeserialization("LabelUnknown-2.json", LabelObject.class, null);
    }


    @Test
    public void testMinimalExtractionResultDeserialization() throws IOException {

        String jsonString = fixture("fixtures/ExtractionResult-minimal.json");
        ExtractionResult extractionResult = om.readValue(jsonString, ExtractionResult.class);

        assertEquals("41ecd4f2-d5a8-11e5-b5c9-17f7e2659337", extractionResult.getId());
        assertEquals(0.99, extractionResult.getConfidence(), 0.01);
        assertEquals(Collections.emptyList(), extractionResult.getExtractions());
        assertEquals(null, extractionResult.getRasterizedPages());
    }

    @Test
    public void testFullExtractionResultDeserialization() throws IOException {

        String jsonString = fixture("fixtures/ExtractionResult-full.json");
        ExtractionResult extractionResult = om.readValue(jsonString, ExtractionResult.class);

        assertEquals("41ecd4f2-d5a8-11e5-b5c9-17f7e2659337", extractionResult.getId());
        assertEquals(0.99, extractionResult.getConfidence(), 0.01);
        assertEquals(1, extractionResult.getExtractions().size());
        assertEquals(null, extractionResult.getRasterizedPages());
    }


    private void testLabelDeserialization(String filename, Class expectedClass, LabelType expectedLabelType) throws IOException {
        String jsonString = fixture("fixtures/" + filename);
        Label label = om.readValue(jsonString, Label.class);
        assertEquals(expectedClass, label.getClass());
        assertEquals(expectedLabelType, label.getType());
    }
}
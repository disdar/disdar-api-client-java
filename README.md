DISDAR API Java Client
======================

A Java client for the DISDAR extraction service.

Usage
-----

The DISDAR extraction service is a RESTful web service that is able to automatically and reliably extract semantic information out of documents. This library provides a simple to use Java client for performing requests to the extraction service and handle the extraction results. More information on this service can be found in the corresponding [API documentation](http://disdar.com/docs.html).

To use the extraction service you simply need to create an document analysis request and send it to the service:

```
#!java

DocumentAnalysisRequest documentAnalysisRequest = new DocumentAnalysisRequest("requestId",
                                                            new URL("http://url.to.document"),
                                                            new URL("http://url.to.callback.listener");

DisdarApiClient disdarApiClient = new DisdarClient("https://api.disdar.com/v1/", "YourApiKey");
disdarApiClient.documentAnalysis(documentAnalysisRequest);
```

The DISDAR API will perform the analysis on the given document and send a `POST` request with the document analysis result to the given callback listener.

The server receiving the callback can use the `DocumentAnalysisResult` class to process the results. This class is configured to deserialize the content of the callback into the corresponding classes using the [Jackson](https://github.com/FasterXML/jackson) library.

Maven Artifacts
---------------

The DISDAR API client is available on Maven Central. To add it to your project simply add the following dependencies to your POM:

```
<dependency>
    <groupId>com.disdar</groupId>
    <artifactId>disdar-api-client-java</artifactId>
    <version>${version}</version>
</dependency>
```

License
-------

Copyright (c) 2015, DISDAR GmbH.

This library is licensed under the New BSD License.

See http://opensource.org/licenses/BSD-3-Clause or the LICENSE file in this repository for the full license text.